from django.urls import path
from . import views

# you can declare routes by using the path function and having 3 arguements within that function.
# 1st arguement - endpoint, 2nd arguement - function to be triggered, 3rd arguement - label for the route
app_name = 'todolist'
urlpatterns = [
    path('', views.index, name = "index"),
    path('register', views.register, name="register"),
    path('change_password', views.change_password, name="change_password"),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),

    path('add_task', views.add_task, name='add_task'),
    path('todoitem/<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),
    path('todoitem/<int:todoitem_id>/edit', views.update_task, name="update_task"),
    path('todoitem/<int:todoitem_id>/delete', views.delete_task, name="delete_task"),

    path('add_event', views.add_event, name="add_event"),
    path('eventitem/<int:eventitem_id>/', views.eventitem, name="vieweventitem"),
    path('eventitem/<int:eventitem_id>/edit/', views.update_event, name='update_event'),
    path('eventitem/<int:eventitem_id>/delete/', views.delete_event, name='delete_event'),
]  