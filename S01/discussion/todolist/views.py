from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone
from django.contrib.auth.hashers import make_password
# from django.template import loader

# Local imports
from .models import ToDoItem, EventItem
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm, AddEventForm, UpdateEventForm

# Create your views here.
def index(request):
    todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
    eventitem_list = EventItem.objects.filter(user_id=request.user.id)
    # output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
    # template = loader.get_template("todolist/index.html")
    # context = {
    #     'todoitem_list' : todoitem_list
    # }
    context = { 
        'todoitem_list' : todoitem_list,
        'eventitem_list' : eventitem_list,
        'user' : request.user
        }
    return render(request, "todolist/index.html", context)
    # return HttpResponse(template.render(context, request))

def todoitem(request, todoitem_id):
    # response = "You are viewing the details of %s"
    # return HttpResponse(response % todoitem_id)
    # the model_to_dict() allows to convert models into dictionaries
    # get() allows to retrieve a record using it primary key(pk)
    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
    return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

def register(request):

    context = {}

    if request.method == 'POST':
        form = RegisterForm(request.POST)

        if form.is_valid() == False:
              form = RegisterForm()

        else:
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']

            duplicates = User.objects.filter(email=email)

            password = make_password(password)

            if not duplicates:
             User.objects.create(username=username, first_name=first_name, last_name=last_name, email=email, password=password)
             return redirect("todolist:index")
        
            else:
                context = {
                    "error" : True
                }

    return render(request, "todolist/register.html", context)



def change_password(request):
    is_user_authenticated = False


    # authenticate the user
    # returns the user object if found and "None" if not found
    user = authenticate(username="johndoe", password="john1234")
    print(user)

    if user is not None:
        authenticated_user = User.objects.get(username="johndoe")
        authenticated_user.set_password("john1")
        authenticated_user.save()
        is_user_authenticated = True

    context = {
        "is_user_authenticated" : is_user_authenticated
    }
    return render(request, "todolist/change_password.html", context)


def login_view(request):
    context ={}

    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid() == False:
            form = LoginForm()

        else:
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

    
            user = authenticate(username=username, password=password)
            context = {
                "username" : username,
                "password" : password
            }
    
            if user is not None:
                # Saves creates a record in the django_session table in the datebase
                login(request, user)
                # redirects the user to the index.html page
                return redirect("todolist:index")
            
            else:
                context = {
                    "error" : True
                }

    return render(request, "todolist/login.html",context)

def logout_view(request):
    logout(request)
    return redirect("todolist:index")

def add_task(request):

	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)

		if form.is_valid() == False:
			form = AddTaskForm()
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			# Checks if a task already exists in the database
			user = request.user
			duplicates = ToDoItem.objects.filter(user=user, task_name=task_name)

			if not duplicates:

				ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id=request.user.id)
				return redirect("todolist:index")
			
			else:

				context = {
					"error": True
				}

	return render(request, "todolist/add_task.html", context)


def update_task(request, todoitem_id):

    todoitem = ToDoItem.objects.filter(pk=todoitem_id)

    context = {
        "user": request.user,
		"todoitem_id": todoitem_id,
		"task_name": todoitem[0].task_name,
		"description": todoitem[0].description,
		"status" : todoitem[0].status
    }

    if request.method == "POST":
        form = UpdateTaskForm(request.POST)

        if request.is_valid() == False:

            form = UpdateTaskForm()

        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']

            if todoitem:
                todoitem[0].task_name = task_name
                todoitem[0].description = description
                todoitem[0].status = status
                todoitem[0].save()
                return redirect("todolist:index")
            
            else:
                context = {
                    "error" : True
                }
    
    return render(request, "todolist/update_task.html", context)


def delete_task(request, todoitem_id):

	todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()

	return redirect("todolist:index")


def add_event(request):

	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)

		if form.is_valid() == False:
			form = AddEventForm()
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']

			user = request.user
			duplicates = EventItem.objects.filter(user=user, event_name=event_name)

			if not duplicates:
				EventItem.objects.create(event_name=event_name, description=description, date_created=timezone.now(), user_id=request.user.id)
				return redirect("todolist:index")
			else:
				context = {
					"error" : True
				}

	return render(request, "todolist/add_event.html", context)



def eventitem(request, eventitem_id):
	eventitem = get_object_or_404(EventItem, pk=eventitem_id)
	return render(request, "todolist/eventitem.html", model_to_dict(eventitem))


def update_event(request, eventitem_id):

	eventitem = EventItem.objects.filter(pk=eventitem_id)

	context = {
		"user" : request.user,
		"eventitem_id" : eventitem_id,
		"event_name" : eventitem[0].event_name,
		"description" : eventitem[0].description,
		"status" : eventitem[0].status
	}

	if request.method == 'POST':
		form = UpdateEventForm(request.POST)

		if form.is_valid() == False:
			form = UpdateEventForm()
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']
		
		if eventitem:
			eventitem[0].event_name = event_name
			eventitem[0].description = description
			eventitem[0].status = status
			eventitem[0].save()
			return redirect("todolist:index")
		else:
			context = {
				"error" : True
			}

	return render(request, 'todolist/update_event.html', context)


def delete_event(request, eventitem_id):

	eventitem = EventItem.objects.filter(pk=eventitem_id).delete()

	return redirect("todolist:index")

