from django.contrib import admin

from .models import ToDoItem, EventItem

admin.site.register(ToDoItem)
admin.site.register(EventItem)


