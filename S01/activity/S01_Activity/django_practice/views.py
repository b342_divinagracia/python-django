from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict


from .models import GroceryItem
from .forms import RegisterForm


# Create your views here.
def index(request):
    groceryitem_list = GroceryItem.objects.all()
    context = { 
        'groceryitem_list' : groceryitem_list,
        'user' : request.user
        }
    return render(request, "django_practice/index.html", context)

def groceryitem(request, groceryitem_id):
    groceryitem = model_to_dict(GroceryItem.objects.get(pk=groceryitem_id))
    return render(request, "django_practice/groceryitem.html", groceryitem)

def register(request):

    context = {}

    if request.method == 'POST':
        form = RegisterForm(request.POST)

        if form.is_valid() == False:
            form = RegisterForm()

        else:
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']

        user = authenticate(username=username, password=password)
        context = {
            "username" : username,
            "password" : password,
            "first_name" : first_name,
            "last_name" : last_name,
            "email" : email
        }

        if user is not None:
            register(request, user)
            return redirect("django_practice:index")
        
        else:
            context ={
                "error" : True
            }

    return render(request, "django_practice/register.html", context)



    # users = User.objects.all()
    # is_user_registered = False
    # context = {
    #     "is_user_registered" : is_user_registered
    # }

    # for indiv_user in users:
    #     if indiv_user.username == "russell17":
    #         is_user_registered = True
    #         break

    # if is_user_registered == False:
    #     user = User()
    #     user.username = "russell17"
    #     user.first_name = "Russell"
    #     user.last_name = "Divinagracia"
    #     user.email = "russ@mail.com"
    #     user.set_password("russ1122")
    #     user.is_staff = False
    #     user.is_active = True
    #     user.save()
    #     context = {
    #         "first_name" : user.first_name,
    #         "last_name" : user.last_name
    #     }
    # return render(request, "django_practice/register.html", context)

def change_password(request):
    is_user_authenticated = False

    user = authenticate(username="russell17", password="russ1122")
    print(user)

    if user is not None:
        authenticated_user = User.objects.get(username="russell17")
        authenticated_user.set_password("russ217")
        authenticated_user.save()
        is_user_authenticated = True

    context = {
        "is_user_authenticated" : is_user_authenticated
    }
    return render(request, "django_practice/change_password.html", context) 

def login_view(request):
    username = "russell17"
    password = "russ217"
    user = authenticate(username=username, password=password)
    context = {
        "is_user_authenticated" : False
    }
    print(user)
    if user is not None:
        login(request, user)
        return redirect("django_practice:index")
    else:
        return render(request, "django_practice/login.html", context)
    
def logout_view(request):
    logout(request)
    return redirect("django_practice:index")
