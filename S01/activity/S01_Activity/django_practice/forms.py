from django import forms

class RegisterForm(forms.Form):
    username = forms.CharField(label="Username", max_length=20)
    first_name = forms.CharField(label="First Name", max_length=20)
    last_name = forms.CharField(label="Last Name", max_length=20)
    email = forms.CharField(label="Email", max_length=30)
    password1 = forms.CharField(label='Password', min_length=20)
    password2 = forms.CharField(label='Confirm password', min_length=8)
