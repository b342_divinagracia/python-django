from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('django_practice/', include('django_practice.urls')),
    path('admin/', admin.site.urls),
]
